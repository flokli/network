{ channels }:

let
  buildSystem = ({ config, system, channel }:
  import (channel + "/nixos") {
    inherit system;
    configuration = config;
  });

in {
  tp = buildSystem {
    config = import /home/flokli/dev/nixos/nixos-configuration/configuration.nix;
    channel = channels.nixpkgs-unstable;
    system = "x86_64-linux";
  };
  vultr1 = buildSystem {
    config = import ./vultr1.nix;
    channel = channels.nixpkgs-unstable;
    system = "x86_64-linux";
  };
  #dunst = buildSystem {
  #  config = import /home/flokli/dev/nixos/nixos-configuration/machines/dunst.nix;
  #  channel = channels.nixpkgs-1903;
  #  system = "armv7l-linux";
  #};
}
