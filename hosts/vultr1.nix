{ config, pkgs, ... }:

{
  imports = [
    ../profiles/hardware-vultr.nix
    ../profiles/openssh.nix
  ];
}
