let
  channelTarball = spec: builtins.fetchTarball {
    url = "https://github.com/${spec.owner}/${spec.repo}/archive/${spec.rev}.tar.gz";
    sha256 = spec.sha256;
  };
  channelTarballFromJSONFile = jsonFile: (channelTarball (builtins.fromJSON (builtins.readFile jsonFile)));


  nixpkgs-1903     = channelTarballFromJSONFile ./nixos-19.03.json;
  nixpkgs-unstable = channelTarballFromJSONFile ./nixos-unstable.json;
in

{
  inherit nixpkgs-1903 nixpkgs-unstable;
}
