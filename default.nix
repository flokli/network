let
  channels = import ./channels/default.nix;
  channelForSystem = { channel, system, config ? {} }:
    import channel { inherit system; inherit config; };

in {
  inherit channels; #pkgs;
  hosts = (import ./hosts) {
    inherit channels;
  };
}
