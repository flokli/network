{ config, pkgs, lib, ... }:

{
  imports = [ ./qemu-guest.nix ];

  boot.loader.grub = {
    enable = true;
    device = "/dev/vda";
  };
  fileSystems."/" = {
    device = "/dev/vda";
    fsType = "btrfs";
  };
}
