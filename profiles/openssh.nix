{ pkgs, config, ... }:

{
  services.openssh.enable = true;
  services.openssh.permitRootLogin = "prohibit-password";
  users.users.root.initialPassword = "";
  users.users.root.openssh.authorizedKeys.keys = let
    k = (import ../keys.nix);
  in with k; [
    flokli_ed25519 flokli_rsa
  ];
}
